﻿using System.Linq;
using NUnit.Framework;

namespace Questionaire.Test
{
    [TestFixture]
    public class AntwortSelectorTests
    {
        [Test]
        public void Test()
        {
            var aufgaben = new []
            {
                new Aufgabe {
                    Id = 1,
                    Optionen = new []{
                        new Option { Id = 1, IstAusgewaehlt = false },
                        new Option { Id = 2, IstAusgewaehlt = true }
                    }
                }
            };

            var result = AntwortSelector.SetzeAuswahl(aufgaben, 1, 1);

            Assert.IsTrue(result.First().Optionen.Single(o => o.Id == 1).IstAusgewaehlt);
            Assert.IsFalse(result.First().Optionen.Single(o => o.Id == 2).IstAusgewaehlt);
        }
    }
}
