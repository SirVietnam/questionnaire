﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Questionaire.Test
{
    [TestFixture]
    public class AufgabenBuilderTests
    {
        [Test]
        public void AufgabenErstellen()
        {
            var zeilen = new[]
            {
                "?Frage 1", "11", "*12", "13",
                "?Frage 2", "21", "*22", "23"
            };

            var result = AufgabenBuilder.AufgabenErstellen(zeilen);

            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void ErstelleAufgabe()
        {
            var aufgabenZeilen = new[]
            {
                "?Frage 2", "21", "*22", "23"
            };

            var result = AufgabenBuilder.ErstelleAufgabe(aufgabenZeilen);

            Assert.AreEqual("Frage 2?", result.Frage);
            Assert.AreEqual(3, result.Optionen.Count());
        }
    }
}
