using System.Collections.Generic;

namespace Questionaire
{
    public struct Aufgabe
    {
        public int Id { get; set; }
        public string Frage { get; set; }
        public IEnumerable<Option> Optionen { get; set;}
    }
}