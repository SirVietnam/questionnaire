namespace Questionaire
{
    public class Option
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IstRichtig { get; set; }
        public bool IstAusgewaehlt { get; set; }
    }
}