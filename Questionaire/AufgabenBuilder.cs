using System;
using System.Collections.Generic;
using System.Linq;

namespace Questionaire
{
    public class AufgabenBuilder
    {
        public static IEnumerable<Aufgabe> AufgabenErstellen(IEnumerable<string> zeilen)
        {
            var gruppen = GruppiereZeilenNachAufgaben(zeilen);
            return InterpretiereGruppenAlsAufgaben(gruppen);
        }

        public static IEnumerable<List<string>> GruppiereZeilenNachAufgaben(IEnumerable<string> zeilen)
        {
            var gruppen = new List<List<string>>();
            foreach (string zeile in zeilen)
            {
                if (zeile.StartsWith("?"))
                {
                    gruppen.Add(new List<string> { zeile });
                }
                else
                {
                    gruppen.Last()?.Add(zeile);
                }
            }
            return gruppen;
        }

        public static IEnumerable<Aufgabe> InterpretiereGruppenAlsAufgaben(IEnumerable<IEnumerable<string>> gruppen)
        {
            var gruppenCount = 0;
            var aufgaben = new List<Aufgabe>();
            foreach (var gruppenZeilen in gruppen)
            {
                var neueAufgabe = ErstelleAufgabe(gruppenZeilen);
                neueAufgabe.Id = gruppenCount++;
                aufgaben.Add(neueAufgabe);
            }
            return aufgaben;
        }

        public static Aufgabe ErstelleAufgabe(IEnumerable<string> aufgabenZeilen)
        {
            var frage = aufgabenZeilen.First();
            var optionen = aufgabenZeilen.Except(new [] {frage});
            var optionId = 0;

            return new Aufgabe
            {
                Frage = $"{frage.TrimStart('?')}?",
                Optionen = optionen.Select(o => new Option
                {
                    Id = optionId++,
                    Text = o.TrimStart('*'),
                    IstRichtig = o.StartsWith("*"),
                })
            };
        }
    }
}