using System.Collections.Generic;

namespace Questionaire
{
    public class Initialisation
    {
        public static IEnumerable<Aufgabe> Start()
        {
            var zeilen = ZeilenReader.ZeilenLesen();
            return AufgabenBuilder.AufgabenErstellen(zeilen);
        }
    }
}