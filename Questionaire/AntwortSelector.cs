﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questionaire
{
    public class AntwortSelector
    {
        public static AufgabenResult AntwortAuswaehlen(IEnumerable<Aufgabe> aufgaben, int aufgabenId, int optionId)
        {
            var modifizierteAufgaben = SetzeAuswahl(aufgaben, aufgabenId, optionId);
            return new AufgabenResult
            {
                Aufgaben = modifizierteAufgaben,
                Fertig = IstFertig(modifizierteAufgaben)
            };
        }

        public static IEnumerable<Aufgabe> SetzeAuswahl(IEnumerable<Aufgabe> aufgaben, int aufgabenId, int optionId)
        {
            var aufgabe = aufgaben.Single(a => a.Id == aufgabenId);
            var optionen = aufgabe.Optionen.ToList();
            optionen.ForEach(o =>
            {
                o.IstAusgewaehlt = o.Id == optionId;
            });
            aufgabe.Optionen = optionen;
            return aufgaben;
        }

        public static bool IstFertig(IEnumerable<Aufgabe> aufgaben)
        {
            throw new NotImplementedException();
        }
    }

    public struct AufgabenResult
    {
        public IEnumerable<Aufgabe> Aufgaben { get; set; }
        public bool Fertig { get; set; }
    }
}
