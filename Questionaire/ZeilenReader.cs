using System.Collections.Generic;
using System.IO;

namespace Questionaire
{
    public class ZeilenReader
    {
        public static IEnumerable<string> ZeilenLesen()
        {
            return File.ReadLines("questionaire.txt");
        }
    }
}