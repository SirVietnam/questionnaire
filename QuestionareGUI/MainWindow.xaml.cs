﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Questionaire;

namespace QuestionareGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            SetAufgaben(Initialisation.Start());

            //Aufgabe neueAufgabe = new Aufgabe();
            //neueAufgabe.Id = 0;
            //neueAufgabe.Frage = "Doofe Frage Typ";
            //List<Option> options = new List<Option>();
            //Option firstOption = new Option();
            //firstOption.Text = "42";
            //firstOption.Selected = true;
            //firstOption.Id = 42;
            //options.Add(firstOption);

            //Option secondOption = new Option();
            //secondOption.Text = "42";
            //secondOption.Selected = true;
            //secondOption.Id = 42;
            //options.Add(secondOption);

            //neueAufgabe.Optionen = options;

            //List<Aufgabe> aufgabenListe = new List<Aufgabe>();
            //aufgabenListe.Add(neueAufgabe);
            //SetAufgaben(aufgabenListe);
        }

        public void SetAufgaben(IEnumerable<Aufgabe> aufgaben)
        {
            AufgabenUC aufgabenPanel = new AufgabenUC();
            aufgabenPanel.SetAufgaben(aufgaben);

            this.MainPanel.Children.Clear();
            this.MainPanel.Children.Add(aufgabenPanel);
        }
    }
}
