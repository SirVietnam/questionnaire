﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuestionareGUI.SubElements
{
    /// <summary>
    /// Interaction logic for MöglicheAntwortUC.xaml
    /// </summary>
    public partial class MöglicheAntwortUC : UserControl
    {
        public bool CheckboxIsChecked { get; set; }
        public string AntwortMoeglichkeitText { get; set; } = "test";

        public MöglicheAntwortUC()
        {
            InitializeComponent();
            DataContext = this;
        }
        
    }
}
