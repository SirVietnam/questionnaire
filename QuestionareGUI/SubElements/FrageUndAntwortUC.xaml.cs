﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Questionaire;
using QuestionareGUI.SubElements;

namespace QuestionareGUI
{
    /// <summary>
    /// Interaction logic for FrageUndAntwortUC.xaml
    /// </summary>
    public partial class FrageUndAntwortUC : UserControl
    {
        private string fragetext;

        public FrageUndAntwortUC()
        {
            InitializeComponent();
            DataContext = this;
        }
        
        internal void SetAufgabe(Aufgabe aufgabe)
        {
            this.Fragetext = aufgabe.Frage;

            foreach (var option in aufgabe.Optionen)
            {
                MöglicheAntwortUC moeglichAntwort = new MöglicheAntwortUC();
                moeglichAntwort.AntwortMoeglichkeitText = option.Text;
                moeglichAntwort.CheckboxIsChecked = option.IstAusgewaehlt;

                MoeglicheAntworten.Children.Add(moeglichAntwort);
            }

        }

        public string Fragetext { get; set; }
    }
}
