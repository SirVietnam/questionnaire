﻿using Questionaire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuestionareGUI
{
    /// <summary>
    /// Interaction logic for AufgabenUC.xaml
    /// </summary>
    public partial class AufgabenUC : UserControl
    {
        public Visibility AufgabenButtonVisibility { get; } = Visibility.Collapsed;

        public AufgabenUC()
        {
            InitializeComponent();
            DataContext = this;
        }


        public void SetAufgaben(IEnumerable<Aufgabe> aufgaben)
        {
            foreach (var aufgabe in aufgaben)
            {
                FrageUndAntwortUC neueAufgabe = new FrageUndAntwortUC();
                neueAufgabe.SetAufgabe(aufgabe);
                AufgabenPanel.Children.Add(neueAufgabe);
            }
        }

    }
}
